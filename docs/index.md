# 이상 탐지 기법과 어플리케이션 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기술과 어플리케이션을 사용하여 이상 탐지를 수행하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./anomaly-detection.md#intro)
1. [이상 탐지란?](./anomaly-detection.md#sec_02)
1. [이상의 종류와 탐지 방법](./anomaly-detection.md#sec_03)
1. [이상 탐지 기법](./anomaly-detection.md#sec_04)
1. [이상 탐지 어플리케이션](./anomaly-detection.md#sec_05)
1. [과제와 향후 방향](./anomaly-detection.md#sec_06)
1. [마치며](./anomaly-detection.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 30 — Anomaly Detection Techniques and Applications](https://ai.gopubby.com/ml-tutorial-30-anomaly-detection-techniques-and-applications-e90d04817639?sk=c0f524e5381789072a7bd1a36fd5cac5)를 편역하였다.
